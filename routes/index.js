var express = require('express');
var router = express.Router();
const helper = require('../config/helper');
var emailConfig = require('../config/email');


//     BASIC INDEX PAGE
//     API URL  http://doaaraam-server.herokuapp.com
router.get('/', (req, res) => {


    let arr = [
        {first:"rajat", last:"saini", age: 17},
        {first:"ashish", last:"saini", age: 16},
        {first:"ruddy", last:"tanwar", age: 15},
        {first:"kapil", last:"saini", age: 16},
        {first:"rohit", last:"tanwar", age: 18}
    ];

    // let newarr = arr.filter( (item) => {
    //     if(item.age <17){
    //         return;
    //     }
    //     return item;
    // })

    // let newarr = arr.filter( (item) => {
    //     return item.age>17
    // });

    let newarr = arr.reduce( ( ageSum, item) => {
        return ageSum + item.age
    }, 0);

    console.log(newarr);

    // helper.transporter.sendMail({
    //     from : 'ruddytanwar@gmail.com',
    //     to: 'ashishsaini3342@gmail.com',
    //     subject: 'Test Email Subject',
    //     html: emailConfig.signUpBody
    //   })
    //   .then(()=>{
    //       console.log("success");
    //   })
    //   .catch((error) => console.log(error));
    res.render('index', { title: "doaaraam"});
});

module.exports = router;
