let express = require('express');
const { database } = require('../config/helper');
let router = express.Router();

//  GET all orders from ORDER_DETAILS table
router.get('/', (req, res) => {
    database.table('order_details')
        .getAll()
        .then((orders) => {
            if(orders.length){
                res.status(200).json({
                    success: true,
                    orders: orders
                });
            }else{
                res.status(400).json({
                    success: false,
                    message: "Failed to locate orders"
                });
            }
        })
        .catch((error) => console.log(error));
});

//  GET A USER ORDER
router.get('/:orderId', (req, res) => {
    const orderId = req.params.orderId;
    database.table('order_details')
        .filter({'id': orderId})
        .getAll()
        .then((orders) => {
            if(orders.length){
                res.status(200).json({
                    success: true,
                    orders: orders
                });
            }else{
                res.status(400).json({
                    success: false,
                    message: "Failed to locate you order"
                });
            }
        })
        .catch((error) => console.log(error));
});

//  Complete Order After Successfull Payment
router.post('/completeorder', (req, res) => {
    const { order_id, price, user_order_id} = req.body;
    let vendorRating = 4;
    let userRating = 4;
    let vendorComment = "Nice vendor";
    let vendorConfirm = "Yes";
    let userConfirm = "Yes";

    database.table('complete_orders')
        .insert({
            order_id: order_id,
            vendor_confirm: vendorConfirm,
            user_confirm: userConfirm,
            final_vendor_price: price,
            vendor_rating: vendorRating,
            user_rating: userRating,
            vendor_comment: vendorComment
        })
        .then( (comOrderId) => {
            if (comOrderId){
                database.table('user_orders')
                    .filter({'id': user_order_id})
                    .update({
                        complete: 'Yes'
                    })
                    .then( (successNum) => {
                        if (successNum){
                            res.status(200).json({
                                success: true,
                                orderDetails: [{
                                    id: comOrderId,
                                    orderID: order_id,
                                    price: price,
                                    vendorRating: vendorRating,
                                    userRating: userRating,
                                    vendorComment: vendorComment
                                }]
                            })
                        } else{
                            res.status(400).json({
                                success: false,
                                message: "Falied to complete your order"
                            })
                        }
                    })
            } else{
                res.status(400).json({
                    success: false,
                    message: "Failed to complete your order"
                })
            }
        })
        .catch( (error) => console.log(error));
});

module.exports = router;